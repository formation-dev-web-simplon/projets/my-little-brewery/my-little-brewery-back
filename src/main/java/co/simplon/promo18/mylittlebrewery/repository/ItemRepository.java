package co.simplon.promo18.mylittlebrewery.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.mylittlebrewery.entity.Item;
import co.simplon.promo18.mylittlebrewery.entity.Record;

@Repository
public class ItemRepository {

  @Autowired
  private DataSource dataSource;

  public List<Item> findAll() {
    List<Item> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT i.*, SUM(r.quantity) AS quantity FROM item i LEFT JOIN record r ON i.id = r.id_item GROUP BY i.id");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Item item = new Item(rs.getInt("id"), rs.getString("name"), rs.getString("brand"),
            rs.getString("type"), new ArrayList<Record>(), rs.getFloat("quantity"));
        list.add(item);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public Item findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT i.*, SUM(r.quantity) AS quantity FROM item i LEFT JOIN record r ON i.id = r.id_item WHERE i.id=? GROUP BY i.id");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Item item = new Item(rs.getInt("id"), rs.getString("name"), rs.getString("brand"),
            rs.getString("type"), new ArrayList<Record>(), rs.getFloat("quantity"));
        return item;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void save(Item item) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO item (name, brand, type) VALUES (?,?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, item.getName());
      stmt.setString(2, item.getBrand());
      stmt.setString(3, item.getType());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        item.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean update(Item item) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("UPDATE item SET name=?, brand=?, type=? WHERE id=?");

      stmt.setString(1, item.getName());
      stmt.setString(2, item.getBrand());
      stmt.setString(3, item.getType());
      stmt.setInt(4, item.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM item WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}
