package co.simplon.promo18.mylittlebrewery.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.mylittlebrewery.entity.Fermentable;
import co.simplon.promo18.mylittlebrewery.entity.Hop;
import co.simplon.promo18.mylittlebrewery.entity.Record;
import co.simplon.promo18.mylittlebrewery.entity.Yeast;

@Repository
public class RecordRepository {

  @Autowired
  private DataSource dataSource;

  public LocalDate convertToLocaldDate(Date date) {
    return date == null ? null : date.toLocalDate();
  }

  public List<Record> findAll() {
    List<Record> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM record");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Record record = new Record(rs.getInt("id"), rs.getDate("date").toLocalDate(),
            rs.getFloat("quantity"), rs.getFloat("conditioning"),
            rs.getDate("use_by_date").toLocalDate(), rs.getFloat("cost"), rs.getString("comment"));
        list.add(record);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<Record> findAllByItemId(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM item i LEFT JOIN record r ON i.id = r.id_item LEFT JOIN fermentable f ON f.id = r.id LEFT JOIN hop h ON h.id = r.id LEFT JOIN yeast y ON y.id = r.id WHERE r.id_item=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        List<Record> records = new ArrayList<>();
        do {
          Record record = new Record(rs.getInt("r.id"), convertToLocaldDate(rs.getDate("r.date")),
              rs.getFloat("r.quantity"), rs.getFloat("r.conditioning"),
              convertToLocaldDate(rs.getDate("r.use_by_date")), rs.getFloat("r.cost"),
              rs.getString("r.comment"));
          switch (rs.getString("i.type")) {
            case "fermentable":
              records.add(new Fermentable(rs.getInt("f.id"), rs.getFloat("f.ebc"), record));
              break;
            case "hop":
              records.add(new Hop(rs.getInt("h.id"), rs.getFloat("h.alpha"),
                  rs.getString("h.shape"), Year.of(rs.getDate("h.harvest").toLocalDate().getYear()),
                  rs.getString("h.origin"), record));
              break;
            case "yeast":
              records.add(new Yeast(rs.getInt("y.id"), rs.getFloat("y.attenuation"), record));
              break;
            default:
              records.add(record);
              break;
          }
        } while (rs.next());

        return records;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public Record findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM item i LEFT JOIN record r ON i.id = r.id_item LEFT JOIN fermentable f ON f.id = r.id LEFT JOIN hop h ON h.id = r.id LEFT JOIN yeast y ON y.id = r.id WHERE r.id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Record record = new Record(rs.getInt("r.id"), convertToLocaldDate(rs.getDate("r.date")),
            rs.getFloat("r.quantity"), rs.getFloat("r.conditioning"),
            convertToLocaldDate(rs.getDate("r.use_by_date")), rs.getFloat("r.cost"),
            rs.getString("r.comment"));
        switch (rs.getString("i.type")) {
          case "fermentable":
            return new Fermentable(rs.getInt("f.id"), rs.getFloat("f.ebc"), record);
          case "hop":
            return new Hop(rs.getInt("h.id"), rs.getFloat("h.alpha"), rs.getString("h.shape"),
                Year.of(rs.getDate("h.harvest").toLocalDate().getYear()), rs.getString("h.origin"),
                record);
          case "yeast":
            return new Yeast(rs.getInt("y.id"), rs.getFloat("y.attenuation"), record);
          default:
            return record;
        }
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void save(Record record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO record (date, quantity, conditioning, use_by_date, cost, comment, id_item) VALUES (?,?,?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setDate(1, Date.valueOf(record.getDate()));
      stmt.setFloat(2, record.getQuantity());
      stmt.setFloat(3, record.getConditioning());
      if (record.getUseByDate() != null) {
        stmt.setDate(4, Date.valueOf(record.getUseByDate()));
      } else {
        stmt.setNull(4, java.sql.Types.DATE);
      }
      if (record.getCost() != null) {
        stmt.setFloat(5, record.getCost());
      } else {
        stmt.setNull(5, java.sql.Types.FLOAT);
      } ;
      stmt.setString(6, record.getComment());
      stmt.setInt(7, record.getItem().getId());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        record.setId(rs.getInt(1));

        if (record instanceof Fermentable) {
          save((Fermentable) record);
        }
        if (record instanceof Hop) {
          save((Hop) record);
        }
        if (record instanceof Yeast) {
          save((Yeast) record);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void save(Fermentable record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO fermentable (id, ebc) VALUES (?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setInt(1, record.getId());
      stmt.setFloat(2, record.getEbc());

      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void save(Hop record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO hop (id, alpha, shape, harvest, origin) VALUES (?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setInt(1, record.getId());
      stmt.setFloat(2, record.getAlpha() != null ? record.getAlpha() : null);
      stmt.setString(3, record.getShape());
      stmt.setString(4, record.getHarvest().toString());
      stmt.setString(5, record.getOrigin());

      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void save(Yeast record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO yeast (id, attenuation) VALUES (?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setInt(1, record.getId());
      stmt.setFloat(2, record.getAttenuation());

      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean update(Record record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE record SET date=?, quantity=?, conditioning=?, use_by_date=? , cost=?, comment=? WHERE id=?");

      stmt.setDate(1, Date.valueOf(record.getDate()));
      stmt.setFloat(2, record.getQuantity());
      stmt.setFloat(3, record.getConditioning());
      if (record.getUseByDate() != null) {
        stmt.setDate(4, Date.valueOf(record.getUseByDate()));
      } else {
        stmt.setNull(4, java.sql.Types.DATE);
      }
      if (record.getCost() != null) {
        stmt.setFloat(5, record.getCost());
      } else {
        stmt.setNull(5, java.sql.Types.FLOAT);
      } ;
      stmt.setString(6, record.getComment());
      stmt.setInt(7, record.getId());

      stmt.executeUpdate();
      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean update(Fermentable record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("UPDATE fermentable SET ebc=? WHERE id=?");

      stmt.setFloat(1, record.getEbc());
      stmt.setInt(2, record.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean update(Hop record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("UPDATE hop SET alpha=?, shape=?, harvest=?, origin=? WHERE id=?");

      stmt.setFloat(1, record.getAlpha());
      stmt.setString(2, record.getShape());
      stmt.setString(3, record.getHarvest().toString());
      stmt.setString(4, record.getOrigin());
      stmt.setInt(5, record.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean update(Yeast record) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("UPDATE yeast SET attenuation=? WHERE id=?");

      stmt.setFloat(1, record.getAttenuation());
      stmt.setInt(2, record.getId());

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM record WHERE id=?");

      stmt.setInt(1, id);

      stmt.executeUpdate();

      return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }
}
