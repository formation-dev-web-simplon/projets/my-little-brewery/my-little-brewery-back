package co.simplon.promo18.mylittlebrewery.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.mylittlebrewery.entity.Item;
import co.simplon.promo18.mylittlebrewery.repository.ItemRepository;

@RestController
@RequestMapping("/api/item")
public class ItemController {

  @Autowired
  private ItemRepository itemRepo;

  @GetMapping
  public List<Item> getAll() {
    return itemRepo.findAll();
  }

  @GetMapping("/{id}")
  public Item getById(@PathVariable int id) {
    Item item = itemRepo.findById(id);
    if (item == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return item;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Item add(@Valid @RequestBody Item item) {
    itemRepo.save(item);
    return item;
  }

  @PutMapping("/{id}")
  public Item update(@RequestBody Item item,@Valid @PathVariable int id) {
    if (id != item.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!itemRepo.update(item)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return itemRepo.findById(item.getId());
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    if (!itemRepo.deleteById(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }
}
