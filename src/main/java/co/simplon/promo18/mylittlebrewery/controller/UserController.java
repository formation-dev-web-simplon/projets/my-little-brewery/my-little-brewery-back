package co.simplon.promo18.mylittlebrewery.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.mylittlebrewery.auth.ChangePasswordDto;
import co.simplon.promo18.mylittlebrewery.entity.User;
import co.simplon.promo18.mylittlebrewery.repository.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {

  @Autowired
  private UserRepository userRepo;

  @Autowired
  private PasswordEncoder encoder;

  @GetMapping("/account")
  public User getAccount(@AuthenticationPrincipal User user) {
    return user;
  }

  @PostMapping
  public User register(@Valid @RequestBody User user) {
    if (userRepo.findByName(user.getName()) != null) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
    }
    user.setId(null);
    user.setRole("ROLE_USER");
    String hashed = encoder.encode(user.getPassword());
    user.setPassword(hashed);
    userRepo.save(user);

    SecurityContextHolder.getContext().setAuthentication(
        new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));

    return user;
  }

  @GetMapping("/{id}")
  public User getOne(@PathVariable int id) {
    User user = userRepo.findById(id);
    if (user == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return user;
  }
  
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    userRepo.deleteById(id);
  }

  @PutMapping("/{id}")
  public void update(@PathVariable int id, @Valid @RequestBody User user) {
    User toUpdate = getOne(id);
    toUpdate.setName(user.getName());
    userRepo.save(toUpdate);
  }
  
  @PatchMapping("/password")
  public void changePassword(@Valid @RequestBody ChangePasswordDto password,
      @AuthenticationPrincipal User user) {
    if (!encoder.matches(password.oldPassword, user.getPassword())) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Passwords don't match");
    }
    user.setPassword(encoder.encode(password.newPassword));
    userRepo.update(user);
  }
}
