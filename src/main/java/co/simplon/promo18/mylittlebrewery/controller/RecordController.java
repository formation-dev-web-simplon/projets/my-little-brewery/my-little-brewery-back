package co.simplon.promo18.mylittlebrewery.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.mylittlebrewery.entity.Fermentable;
import co.simplon.promo18.mylittlebrewery.entity.Hop;
import co.simplon.promo18.mylittlebrewery.entity.Record;
import co.simplon.promo18.mylittlebrewery.entity.Yeast;
import co.simplon.promo18.mylittlebrewery.repository.RecordRepository;

@RestController
@RequestMapping("/api/record")
public class RecordController {

  @Autowired
  private RecordRepository recordRepo;

  @GetMapping
  public List<Record> getAll() {
    return recordRepo.findAll();
  }

  @GetMapping("/item/{id}")
  public List<Record> getByItemId(@PathVariable int id) {
    return recordRepo.findAllByItemId(id);
  }

  @GetMapping("/{id}")
  public Record getById(@PathVariable int id) {
    Record record = recordRepo.findById(id);
    if (record == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return record;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Record add(@Valid @RequestBody Record record) {
    recordRepo.save(record);
    return record;
  }

  @PutMapping("/{id}")
  public Record update(@Valid @RequestBody Record record, @PathVariable int id) {
    if (id != record.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!recordRepo.update(record)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    if (record instanceof Fermentable) {
      if (!recordRepo.update((Fermentable) record)) {
        recordRepo.save((Fermentable) record);
      }
    }
    if (record instanceof Hop) {
      if (!recordRepo.update((Hop) record)) {
        recordRepo.save((Hop) record);
      }
    }
    if (record instanceof Yeast) {
      if (!recordRepo.update((Yeast) record)) {
        recordRepo.save((Yeast) record);
      }
    }
    return recordRepo.findById(record.getId());
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    if (!recordRepo.deleteById(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }
}
