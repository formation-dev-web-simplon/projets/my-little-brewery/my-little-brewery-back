package co.simplon.promo18.mylittlebrewery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyLittleBreweryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyLittleBreweryApplication.class, args);
	}

}
