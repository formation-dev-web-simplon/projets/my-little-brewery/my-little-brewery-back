package co.simplon.promo18.mylittlebrewery.entity;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;

public class Fermentable extends Record {
  private Integer id;
  @NotNull
  private Float ebc;

  public Fermentable() {}

  public Fermentable(Float ebc, Record record) {
    super(record.getDate(), record.getQuantity(), record.getConditioning(), record.getUseByDate(),
        record.getCost(), record.getComment());
    this.ebc = ebc;
  }

  public Fermentable(Integer id, Float ebc, Record record) {
    super(record.getDate(), record.getQuantity(), record.getConditioning(), record.getUseByDate(),
        record.getCost(), record.getComment());
    this.id = id;
    this.ebc = ebc;
  }

  public Fermentable(Float ebc, LocalDate date, Float quantity, Float conditioning,
      LocalDate useByDate, Float cost, String comment) {
    super(date, quantity, conditioning, useByDate, cost, comment);
    this.ebc = ebc;
  }

  public Fermentable(Integer id, Float ebc, LocalDate date, Float quantity,
      Float conditioning, LocalDate useByDate, Float cost, String comment) {
    super(id, date, quantity, conditioning, useByDate, cost, comment);
    this.id = id;
    this.ebc = ebc;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Float getEbc() {
    return ebc;
  }

  public void setEbc(Float ebc) {
    this.ebc = ebc;
  }
}
