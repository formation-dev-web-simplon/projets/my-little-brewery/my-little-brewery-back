package co.simplon.promo18.mylittlebrewery.entity;

import java.time.LocalDate;
import java.time.Year;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

public class Hop extends Record {
  private Integer id;
  @NotNull
  private Float alpha;
  private String shape;
  @NotNull
  @PastOrPresent
  private Year harvest;
  @NotBlank
  private String origin;

  public Hop() {}

  public Hop(Float alpha, String shape, Year harvest, String origin, Record record) {
    super(record.getDate(), record.getQuantity(), record.getConditioning(), record.getUseByDate(),
        record.getCost(), record.getComment());
    this.alpha = alpha;
    this.shape = shape;
    this.harvest = harvest;
    this.origin = origin;
  }

  public Hop(Integer id, Float alpha, String shape, Year harvest, String origin, Record record) {
    super(record.getDate(), record.getQuantity(), record.getConditioning(), record.getUseByDate(),
        record.getCost(), record.getComment());
    this.id = id;
    this.alpha = alpha;
    this.shape = shape;
    this.harvest = harvest;
    this.origin = origin;
  }

  public Hop(Float alpha, String shape, Year harvest, String origin, LocalDate date,
      Float quantity, Float conditioning, LocalDate useByDate, Float cost, String comment) {
    super(date, quantity, conditioning, useByDate, cost, comment);
    this.alpha = alpha;
    this.shape = shape;
    this.harvest = harvest;
    this.origin = origin;
  }

  public Hop(Integer id, Float alpha, String shape, Year harvest, String origin,
      LocalDate date, Float quantity, Float conditioning, LocalDate useByDate, Float cost,
      String comment) {
    super(id, date, quantity, conditioning, useByDate, cost, comment);
    this.id = id;
    this.alpha = alpha;
    this.shape = shape;
    this.harvest = harvest;
    this.origin = origin;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Float getAlpha() {
    return alpha;
  }

  public void setAlpha(Float alpha) {
    this.alpha = alpha;
  }

  public String getShape() {
    return shape;
  }

  public void setShape(String shape) {
    this.shape = shape;
  }

  public Year getHarvest() {
    return harvest;
  }

  public void setHarvest(Year harvest) {
    this.harvest = harvest;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

}
