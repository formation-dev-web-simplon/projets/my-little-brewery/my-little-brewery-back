package co.simplon.promo18.mylittlebrewery.entity;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;

public class Yeast extends Record {
  private Integer id;
  @NotNull
  private Float attenuation;

  public Yeast() {}

  public Yeast(float attenuation, Record record) {
    super(record.getDate(), record.getQuantity(), record.getConditioning(), record.getUseByDate(),
        record.getCost(), record.getComment());
    this.attenuation = attenuation;
  }

  public Yeast(Integer id, float attenuation, Record record) {
    super(record.getDate(), record.getQuantity(), record.getConditioning(), record.getUseByDate(),
        record.getCost(), record.getComment());
    this.id = id;
    this.attenuation = attenuation;
  }

  public Yeast(float attenuation, LocalDate date, Float quantity, Float conditioning,
      LocalDate useByDate, Float cost, String comment) {
    super(date, quantity, conditioning, useByDate, cost, comment);
    this.attenuation = attenuation;
  }

  public Yeast(Integer id, float attenuation, LocalDate date, Float quantity,
      Float conditioning, LocalDate useByDate, Float cost, String comment) {
    super(id, date, quantity, conditioning, useByDate, cost, comment);
    this.id = id;
    this.attenuation = attenuation;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public float getAttenuation() {
    return attenuation;
  }

  public void setAttenuation(float attenuation) {
    this.attenuation = attenuation;
  }

  public void setAttenuation(Float attenuation) {
    this.attenuation = attenuation;
  }
}
