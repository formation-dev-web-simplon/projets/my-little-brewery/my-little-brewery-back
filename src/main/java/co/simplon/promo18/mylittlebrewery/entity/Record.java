package co.simplon.promo18.mylittlebrewery.entity;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type", defaultImpl = Record.class)
@JsonSubTypes({@Type(value = Fermentable.class, name = "fermentable"),
    @Type(value = Hop.class, name = "hop"), @Type(value = Yeast.class, name = "yeast")})
public class Record {
  private Integer id;
  @PastOrPresent
  @NotNull
  private LocalDate date;
  @NotNull
  private Float quantity;
  @NotNull
  private Float conditioning;
  private LocalDate useByDate;
  private Float cost;
  private String comment;
  private Item item;

  public Record() {}

  public Record(LocalDate date, Float quantity, Float conditioning, LocalDate useByDate, Float cost,
      String comment) {
    this.date = date;
    this.quantity = quantity;
    this.conditioning = conditioning;
    this.useByDate = useByDate;
    this.cost = cost;
    this.comment = comment;
  }

  public Record(Integer id, LocalDate date, Float quantity, Float conditioning, LocalDate useByDate,
      Float cost, String comment) {
    this.id = id;
    this.date = date;
    this.quantity = quantity;
    this.conditioning = conditioning;
    this.useByDate = useByDate;
    this.cost = cost;
    this.comment = comment;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Float getQuantity() {
    return quantity;
  }

  public void setQuantity(Float quantity) {
    this.quantity = quantity;
  }

  public Float getConditioning() {
    return conditioning;
  }

  public void setConditioning(Float conditioning) {
    this.conditioning = conditioning;
  }

  public LocalDate getUseByDate() {
    return useByDate;
  }

  public void setUseByDate(LocalDate useByDate) {
    this.useByDate = useByDate;
  }

  public Float getCost() {
    return cost;
  }

  public void setCost(Float cost) {
    this.cost = cost;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Item getItem() {
    return item;
  }

  public void setItem(Item item) {
    this.item = item;
  }

  @Override
  public String toString() {
    return "Record [comment=" + comment + ", conditioning=" + conditioning + ", cost=" + cost
        + ", date=" + date + ", id=" + id + ", item=" + item + ", quantity=" + quantity
        + ", useByDate=" + useByDate + "]";
  }
}
