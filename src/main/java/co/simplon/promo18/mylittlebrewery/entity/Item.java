package co.simplon.promo18.mylittlebrewery.entity;

import java.util.List;
import javax.validation.constraints.NotBlank;

public class Item {
  private Integer id;
  @NotBlank
  private String name;
  @NotBlank
  private String brand;
  private String type;
  private List<Record> records;
  private Float quantity;

  public Item() {}

  public Item( String name, String brand, String type, List<Record> records,
      Float quantity) {
    this.name = name;
    this.brand = brand;
    this.type = type;
    this.records = records;
    this.quantity = quantity;
  }
  public Item(Integer id, String name, String brand, String type, List<Record> records,
      Float quantity) {
    this.id = id;
    this.name = name;
    this.brand = brand;
    this.type = type;
    this.records = records;
    this.quantity = quantity;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<Record> getRecords() {
    return records;
  }

  public void setRecords(List<Record> records) {
    this.records = records;
  }

  public Float getQuantity() {
    return quantity;
  }

  public void setQuantity(Float quantity) {
    this.quantity = quantity;
  }

  @Override
  public String toString() {
    return "Item [brand=" + brand + ", id=" + id + ", name=" + name + ", quantity=" + quantity
        + ", records=" + records + ", type=" + type + "]";
  }

}
