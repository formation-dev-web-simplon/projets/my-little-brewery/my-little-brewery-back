package co.simplon.promo18.mylittlebrewery.auth;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ChangePasswordDto {
  public String oldPassword;
  @NotBlank
  @Size(min = 4, max = 64)
  public String newPassword;
}
