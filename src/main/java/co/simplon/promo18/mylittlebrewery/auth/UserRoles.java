package co.simplon.promo18.mylittlebrewery.auth;

public enum UserRoles {
  ROLE_USER,
  ROLE_ADMIN
}