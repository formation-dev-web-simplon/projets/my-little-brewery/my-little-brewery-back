-- Active: 1648455578008@@127.0.0.1@3306@my_little_brewery
-- CREATE DATABASE my_little_brewery;
DROP TABLE IF EXISTS fermentable;
DROP TABLE IF EXISTS hop;
DROP TABLE IF EXISTS yeast;
DROP TABLE IF EXISTS record;
DROP TABLE IF EXISTS item;

CREATE TABLE item (
  id SMALLINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  brand VARCHAR(64),
  type VARCHAR(64)
);

CREATE TABLE record (
  id SMALLINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  date DATE NOT NULL,
  quantity FLOAT NOT NULL,
  conditioning FLOAT NOT NULL,
  use_by_date DATE,
  cost FLOAT,
  COMMENT TEXT,
  id_item SMALLINT UNSIGNED NOT NULL,
  CONSTRAINT FK_record_item FOREIGN KEY (id_item) REFERENCES item(id) ON DELETE CASCADE
);

CREATE TABLE fermentable (
  id SMALLINT UNSIGNED NOT NULL PRIMARY KEY,
  ebc FLOAT UNSIGNED,
  CONSTRAINT FK_fermentable_record FOREIGN KEY (id) REFERENCES record(id) ON DELETE CASCADE
);

CREATE TABLE hop (
  id SMALLINT UNSIGNED NOT NULL PRIMARY KEY,
  alpha FLOAT,
  shape VARCHAR(32),
  harvest YEAR,
  origin VARCHAR(64),
  CONSTRAINT FK_hop_record FOREIGN KEY (id) REFERENCES record(id) ON DELETE CASCADE
);

CREATE TABLE yeast (
  id SMALLINT UNSIGNED NOT NULL PRIMARY KEY,
  attenuation FLOAT,
  CONSTRAINT FK_yeast_record FOREIGN KEY (id) REFERENCES record(id) ON DELETE CASCADE
);

INSERT INTO
  item (name, brand, type)
VALUES
  (
    'Malt Pilsen',
    'Malterie du Château',
    'fermentable'
  ),
  (
    'Malt Pale Ale',
    'Malterie du Château',
    'fermentable'
  ),
  (
    'Malt Vienna',
    'Malterie du Château',
    'fermentable'
  ),
  ('Aramis', 'Brewferm', 'hop'),
  ('Magnum', 'Brewferm', 'hop'),
  ('Wai-Iti', 'Brewferm', 'hop'),
  (
    'SafAle US-05 (11,5g)',
    'Fermentis',
    'yeast'
  ),
  (
    'LalBrew Premium BRY-97 (11g)',
    'Lallemand',
    'yeast'
  ),
  (
    'M42 New World Strong Ale (10g)',
    'Mangrove Jack\'s',
    'yeast'
  );

INSERT INTO
  record (
    date,
    quantity,
    conditioning,
    use_by_date,
    cost,
    comment,
    id_item
  )
VALUES
  (
    '2021-11-25',
    2.5,
    5,
    '2023-06-05',
    1.84,
    '',
    1
  ),
  (
    '2022-01-12',
    1,
    1,
    '2024-03-16',
    1.84,
    '',
    1
  ),
  (
    '2022-02-12',
    5,
    5,
    '2024-11-01',
    1.92,
    '',
    2
  ),
  (
    '2022-01-13',
    2.5,
    2.5,
    '2024-04-25',
    2,
    '',
    3
  ),
  (
    '2022-04-12',
    300,
    100,
    NULL,
    28.5,
    '1 sachet ouvert',
    4
  ),
  (
    '2022-04-12',
    400,
    100,
    NULL,
    21.8,
    '1 sachet ouvert',
    5
  ),
  (
    '2022-04-12',
    200,
    100,
    NULL,
    74.4,
    '',
    6
  ),
  (
    '2022-01-15',
    2,
    1,
    '2022-12-05',
    2.47,
    '',
    7
  ),
  (
    '2022-02-15',
    1,
    1,
    '2023-06-07',
    2.34,
    '',
    7
  ),
  (
    '2022-03-15',
    3,
    1,
    '2024-03-21',
    2.51,
    '',
    7
  ),
  (
    '2022-01-12',
    4,
    1,
    '2023-05-28',
    3,
    '',
    8
  ),
  (
    '2022-01-12',
    1,
    1,
    '2023-09-17',
    3.15,
    '',
    9
  );

INSERT INTO
  fermentable (ebc, id)
VALUES
  (3.5, 1),
  (3.5, 2),
  (7, 3),
  (5.5, 4);

INSERT INTO
  hop (
    alpha,
    shape,
    harvest,
    origin,
    id
  )
VALUES
  (
    5.6,
    'Pellet',
    '2020',
    'France',
    5
  ),
  (
    14,
    'Pellet',
    '2020',
    'Allemagne',
    6
  ),
  (
    2.4,
    'Pellet',
    '2020',
    'Nouvelle-Zélande',
    7
  );

INSERT INTO
  yeast (attenuation, id)
VALUES
  (77, 8),
  (77, 9),
  (77, 10),
  (76.5, 11),
  (79.5, 12);

DROP TABLE IF EXISTS user;

CREATE TABLE user (
  id SMALLINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  password VARCHAR(64),
  role VARCHAR(64)
);  

INSERT INTO
  user (name, password, role)
VALUES
  (
    'admin',
    '$2a$12$jUJh3U0vLmkRLvhp3NWLdeeXX4pbzBMtUdX15/dWFuzOqtK2Lc1IW',
    'ROLE_ADMIN'
  )